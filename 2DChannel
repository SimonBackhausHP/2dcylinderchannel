#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 11:23:08 2020

@author: simon
"""

from dolfin import *
# To define the number of threads available on your computer
#parameters['num_threads'] = 4
# To create the mesh
import mshr
# To plot respectable graphs/figures
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np



def build_space(N_circle, N_channel, N_square, u_in):
    #Data preparation for benchmark, Return function space and list of
    #boundary conditions and surface measure on the cylinder and square
    
    #Definition of domain
    center = Point(0.05, 0.8)           #Center point of the circle
    radius = 0.02                       #Radius of the circle
    L = 0.1                             #Length of the cylinder channel (x-axis)
    W = 1.0                             #Width of the cylinder channel (y-axis)
    Sx1 = 0.03                          #Coordinate in x-direction rectangular obstracle
    Sx2 = 0.07                          #Coordinate in x-direction rectangular obstracle
    Sy1 = 0.3                           #Coordinate in y-direction rectangular obstracle
    Sy2 = 0.4                           #Coordinate in y-direction rectangular obstracle
   
    #Define geometry as rectangle (cylinder channel) - circle - small rectangle
    geometry = mshr.Rectangle(Point(0.0, 0.0), Point(L, W)) \
                             - mshr.Circle(center, radius, N_circle) \
                             - mshr.Rectangle(Point(Sx1, Sy1), Point(Sx2, Sy2))
    
    #Build the mesh, N_channel will be defined afterwards
    mesh = mshr.generate_mesh(geometry, N_channel)
    
    #Save the mesh in the file name cylinder
    File("results2DNS/cylinder.pvd") << mesh
    
    #Plot mesh, you can plot the mesh if required, however the message
    #NotImplementedError: It is not currently possible to manually set the aspect on 3D axes
    #can be solved by installing matplotlib3.0.3 or by 
 #   plot(mesh, title = 'Mesh of the channel')

    

    #Construction of facet markers
    bndry = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
    for f in facets(mesh):
        mp = f.midpoint()
        if near(mp[1], 0.0):    #inflow
            bndry[f] = 1
        elif near(mp[1], W):    #outflow
            bndry[f] = 2
        elif near(0, mp[0]) or near(L, mp[0]):  #Walls
            bndry[f] = 0
        elif mp.distance(center) <= radius:     #Cylinder
            bndry[f] = 5
        elif near(Sx1, Sy1) or near(Sx2, Sy2):  #Square/Rectangular
            bndry[f] = 7

    #Build function spaces (Taylor-Hood)
    P2 = VectorElement("P", mesh.ufl_cell(), 2)
    P1 = FiniteElement("P", mesh.ufl_cell(), 1)
    TH = MixedElement([P2, P1])
    W = FunctionSpace(mesh, TH)
            
    #Prepare Dirichlet boundary conditions
    #Boundary condition for the cylinder channel at the walls
    bc_walls = DirichletBC(W.sub(0), (0,0), bndry, 3)
    #Boundary condition for the cylinder channel
    bc_cylinder = DirichletBC(W.sub(0), (0,0), bndry, 5)
    #Boundary condition for the rectangular/square       
    bc_square = DirichletBC(W.sub(0), (0,0), bndry, 7)
    #Boundary condition for the inflow
    bc_in = DirichletBC(W.sub(0), u_in, bndry, 1)
    #Boundary conditions
    bcs = [bc_cylinder, bc_walls, bc_square, bc_in]

    # Prepare surface measure on cylinder
    ds_circle = Measure("ds", subdomain_data=bndry, subdomain_id=5)
    
    return W, bcs, ds_circle

    #Prepare surface measure on square
    ds_square = Measure('ds', subdomain_data=bndry, subdomain_id=7)
   
    return W, bcs, ds_square

def solve_navier_stokes(W, nu, bcs):
    #Solves the steady Navier-Stokes &returns the solution
    #Define variational forms
    v, q = TestFunctions(W)
    w = Function(W)
    u, p = split(w)
    F = nu*inner(grad(u), grad(v))*dx + dot(dot(grad(u), u), v)*dx \
        -p*div(v)*dx - q*div(u)*dx
    
    #Solve the problem
    solve(F == 0, w, bcs)
    
    return w

def solve_unsteady_navier_stokes(W, nu, bcs, T, dt, theta):
    #Solve unsteady Navier-Stokes and write the results into a file
    #Current and old solution
    w = Function(W)
    u, p = split(w)
    
    w_old = Function(W)
    u_old, p_old = split(w_old)
    
    #Define variational forms
    v, q = TestFunctions(W)
    F = (   Constant(1/dt)*dot(u-u_old, v)
            + Constant(theta)*nu*inner(grad(u), grad(v))
            + Constant(theta)*dot(dot(grad(u), u), v)
            + Constant(1-theta)*nu*inner(grad(u), grad(v))
            + Constant(1-theta)*dot(dot(grad(u_old), u_old), v)
            - p*div(v)
            - q*div(u)
        )*dx
    J = derivative(F, w)
    
    #Create solver
    problem = NonlinearVariationalProblem(F, w, bcs, J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters['newton_solver']['linear_solver'] = 'mumps'
    
    #Create velocity file of the unsteady Navier-Stokes
    f = XDMFFile('velocity_unsteady_navier_stokes.xdmf')
    u, p = w.split()
    
    #Performing time-stepping
    t = 0
    while t < T:
        w_old.vector()[:] = w.vector()
        solver.solve()
        t += dt
        f.write(u, t)
        
def save_and_plot(w, name):
    
    #Saves and plots the provided solution using the given name
    u, p = w.split()
    
    #Store to file
    with XDMFFile("results_{}/u.xdmf".format(name)) as f:
        f.write(u)
    with XDMFFile("results_{}/p.xdmf".format(name)) as f:
        f.write(p)
        
    # Plot

    plt.figure()
    pl = plot(u, title='velocity {}'.format(name))
    plt.colorbar(pl)
    plt.figure() 
    #In case you prefer the usual 
#   pl = plot(p, title='pressure {}'.format(name))
    pl = plot(p, mode='warp', title='pressure {}'.format(name))
    plt.colorbar(pl)
    
    #Save the velocity in the file channelU
    File("results2DNS/channelU.pvd") << u

#In case you want to analyse the circle please remove the #
#def postprocess(w, nu, ds_circle)
    #Returns lift, drag and the pressure difference
#    u, p = w.split()
#    
    #Report drag and lift
#    n = FacetNormal(w.function_space().mesh())
#    force = -p*n + nu*dot(grad(u), n)
#    F_D = assemble(-force[1]*ds_circle)
#    F_L = assemble(-force[0]*ds_circle)
    
#    U_mean = 0.3
#    L = 0.1
#    C_D = 2/(U_mean**2*L)*F_D
#    C_L = 2/(U_mean**2*L)*F_L
    
    #Report pressure difference
#    a_1 = Point(0.5, 0.23)
#    a_2 = Point(0.5, 0.43)
#    try:
#        p_diff = p(a_1) - p(a_2)
#    except RuntimeError:
#        p_diff = 0
    
 #   return C_D, C_L, p_diff

#Analysis of the rectangular obstacle in terms of drag, lift coefficient and pressure difference
def postprocess(w, nu, ds_square):
    #Returns lift, drag and the pressure difference
    u, p = w.split()
    
    #Report drag and lift
    n = FacetNormal(w.function_space().mesh())
    force = -p*n + nu*dot(grad(u), n)
    F_D = assemble(-force[1]*ds_square)
    F_L = assemble(-force[0]*ds_square)
    
    U_mean = 0.3
    L = 0.1
    C_D = 2/(U_mean**2*L)*F_D
    C_L = 2/(U_mean**2*L)*F_L
    
    #Report pressure difference
    a_1 = Point(0.07, 0.3)     #For the small rectangluar
    a_2 = Point(0.03, 0.4)     #For the small rectangular
    try:
        p_diff = p(a_1) - p(a_2)
    except RuntimeError:
        p_diff = 0
    
    return C_D, C_L, p_diff

def tasks_1_2_3_4():
    u_in = Expression(("0.0", "4.0*U*x[0]*(L-x[0])/(L*L)"), degree=2, U=0.3, L = 0.1)
    nu = Constant(0.001)
    
    #Discretisaiton of parameters
    N_circle = 150
    N_square = 150
    N_channel = 150
    
    #Prepare function space, BCs and measure on circle
    #Please remove the # if you want to analyse the circle
 #   W, bcs, ds_circle = build_space(N_circle, N_square, N_channel, u_in)
    
    #Prepare function space, BCs and measure on square
    W, bcs, ds_square = build_space(N_circle, N_square, N_channel, u_in)
     
    #Solve Navier-Stokes
    w = solve_navier_stokes(W, nu, bcs)
    save_and_plot(w, 'navier-stokes')
    
    #Open and hold plot windows
    plt.show()
    
def tasks_5_6():
    #Run convergence analysis of drag and lift
    
    #Problem data
    u_in = Expression(("0.0", "4.0*U*x[0]*(L-x[0])/(L*L)"), degree=2, U=0.3, L = 0.1)
    nu = Constant(0.001)
    
    #Push log level to silence DOLFIN
    old_level = get_log_level()
    warning = LogLevel.WARNING if cpp.__version__> '2017.2.0' else WARNING
    set_log_level(warning)
    
    #Elegant way to visualise in the number of DOFs, C_D, C_L, pressure difference 
    fmt_header = "{:10s} | {:10s} | {:10s} | {:10s} | {:10s} | {:10s}"
    fmt_row = "{:10d} | {:10d} | {:10d} | {:10.4f} | {:10.4f} | {:10.6f}"    
    # Print table header
    print(fmt_header.format("N_channel", "N_square", "#dofs", "C_D", "C_L", "p_diff"))
                            
    # Solve on series of meshes
    for N_channel in [150]:
        for N_square in [N_channel]:
            
            N_circle = 150
        #   N_square =32    

            # Prepare function space, BCs and measure on circle
       #     W, bcs, ds_circle = build_space(N_circle, N_square, N_channel, u_in)
            
            #Preapre function space, BCs and measure on square
            W, bcs, ds_square = build_space(N_circle, N_square, N_channel, u_in)

            # Solve Navier-Stokes
            w = solve_navier_stokes(W, nu, bcs)

            # Compute drag, lift
            #In case you want to analyse the circle please remove the #
      #      C_D, C_L, p_diff = postprocess(w, nu, ds_circle)
      #      print(fmt_row.format(N_channel, N_circle, W.dim(), C_D, C_L, p_diff))
      
            #Compute drag, lift for square
            C_D, C_L, p_diff = postprocess(w, nu, ds_square)
            print(fmt_row.format(N_channel, N_square, W.dim(), C_D, C_L, p_diff))
             
    # Pop log level
    set_log_level(old_level)    
                        
    
def task_7():
    #solve unsteady Navier-Stokes to resolve Karman vortex street and save to file
    
    #Problem data
    u_in = Expression(("0.0", "4.0*U*x[0]*(L-x[0])/(L*L)"), degree=2, U=0.3, L = 0.1)
    nu = Constant(0.001)
    T=10
    
    #Discretisation parameter
    N_circle = 150
    N_square = 150
    N_channel = 150
    theta = 0.5
    dt = 0.1
    
    #Prepare function space, BCs and measure on circle
   # W, bcs, ds_circle = build_space(N_circle, N_square, N_channel, u_in)
    
    #Prepare function space, BCs and measure on square
    W, bcs, ds_square = build_space(N_circle, N_square, N_channel, u_in)
    
    #Solve unsteady Navier-Stokes
    solve_unsteady_navier_stokes(W, nu, bcs, T, dt, theta)
    
if __name__ == "__main__":
    tasks_1_2_3_4()
    tasks_5_6()
    task_7()
    

